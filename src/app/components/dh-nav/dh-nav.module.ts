import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelsService } from '../../services/channels/channels.service';
import { MatButtonModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { DhNavComponent } from './dh-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DhChannelListModule } from '../dh-channel-list/dh-channel-list.module';
import { DhNavResolver } from './dh-nav.resolver';
import { TitleService } from '../../services/title/title.service';
import { DhChatComponent } from '../dh-chat/dh-chat.component';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    component: DhChatComponent,
    resolve: {
        resolver: DhNavResolver
    }
}, {
    path: ':id',
    loadChildren: '../dh-chat/dh-chat.module#DhChatModule',
    resolve: {
        resolver: DhNavResolver
    }
}];
@NgModule({
    declarations: [
        DhNavComponent,
    ],
    imports: [
        CommonModule,
        MatListModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        DhChannelListModule,
        RouterModule.forChild(routes),
    ],
    providers: [
        DhNavResolver,
        ChannelsService,
        TitleService
    ],
    exports: [
        DhNavComponent
    ]
})
export class DhNavModule {
}
