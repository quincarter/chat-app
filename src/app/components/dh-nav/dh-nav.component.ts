import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ChannelsService } from '../../services/channels/channels.service';
import { TitleService } from '../../services/title/title.service';

@Component({
    selector: 'dh-nav',
    templateUrl: './dh-nav.component.html',
    styleUrls: ['./dh-nav.component.scss']
})
export class DhNavComponent implements OnInit, OnDestroy{

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    public channels$: Observable<any>;

    private channelListSubscription: Subscription;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private channelService: ChannelsService,
        private titleService: TitleService,
    ) {
    }

    ngOnInit(): void {
        this.getChannelListSubscription();
    }

    ngOnDestroy(): void {
        this.channelListSubscription.unsubscribe();
    }

    public setTitle(title: string) {
        this.titleService.setTitle(title);
        this.channelService.getSpecificChannel(title);
    }

    private getChannelListSubscription(): void {
        this.channelListSubscription = this.channelService.channelList$.subscribe(channels => {
            this.channels$ = channels;
        });
    }
}
