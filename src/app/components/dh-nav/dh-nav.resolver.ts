import { Injectable } from '@angular/core';
import { ActivatedRoute, Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { ChannelsService } from '../../services/channels/channels.service';

@Injectable()
export class DhNavResolver implements Resolve<any> {

    constructor(
        private channelsService: ChannelsService,
        private route: ActivatedRoute,
    ) {
    }

    _init(): void {
        this.getChannels();
        if (this.route.snapshot.paramMap.get('id')) {
            this.getSpecificChannel(this.route.snapshot.paramMap.get('id'));
        }
    }

    resolve(): Observable<any[]> | Promise<any[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this._init(),
            ]).then(data => {
                resolve(data);
            }, reject);
        });
    }

    private getChannels(): void {
        this.channelsService.getChannelList();
    }

    private getSpecificChannel(id): void {
        this.channelsService.getSpecificChannel(id);
    }
}
