import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelsService } from '../../services/channels/channels.service';
import { MatInputModule, MatListModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { DhChatComponent } from './dh-chat.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    component: DhChatComponent
}];
@NgModule({
    declarations: [
        DhChatComponent,
    ],
    imports: [
        CommonModule,
        MatInputModule,
        MatListModule,
        RouterModule.forChild(routes),
        FormsModule,
    ],
    providers: [
        ChannelsService
    ],
    exports: [
        DhChatComponent
    ]
})
export class DhChatModule {
}
