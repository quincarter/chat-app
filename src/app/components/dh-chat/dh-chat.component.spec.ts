import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DhChatComponent } from './dh-chat.component';

describe('DhChatComponent', () => {
  let component: DhChatComponent;
  let fixture: ComponentFixture<DhChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DhChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DhChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
