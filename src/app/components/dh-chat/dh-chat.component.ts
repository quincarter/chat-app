import { Component, DoCheck, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { ChannelsService } from '../../services/channels/channels.service';
import { IMessageContent } from '../../models/channels';

@Component({
    selector: 'dh-chat',
    templateUrl: './dh-chat.component.html',
    styleUrls: ['./dh-chat.component.scss']
})
export class DhChatComponent implements OnInit, OnDestroy, OnChanges {

    chat$: Observable<any>;
    newMsg: string;
    source$: Observable<any>;

    private sourceSubscription: Subscription;

    constructor(
        public cs: ChannelsService,
        private route: ActivatedRoute,
        public auth: AuthService
    ) {
    }

    ngOnInit() {
        this.getSourceSubscription();
        const chatId = this.route.snapshot.paramMap.get('id');
        this.cs.getSpecificChannel(chatId);
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.route.snapshot.paramMap.get('id');
    }

    ngOnDestroy(): void {
        this.sourceSubscription.unsubscribe();
    }

    submit(chatId) {
        this.cs.sendMessage(chatId, this.newMsg);
        this.newMsg = '';
    }

    trackByCreated(i, msg) {
        return msg.createdAt;
    }

    private getSourceSubscription(): void {
        this.sourceSubscription = this.cs.selectedChannel$.subscribe(data => {
            this.source$ = data;
        });
    }
}
