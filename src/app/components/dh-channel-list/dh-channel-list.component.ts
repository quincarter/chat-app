import { Component, Input } from '@angular/core';

@Component({
    selector: 'dh-channel-list',
    templateUrl: './dh-channel-list.component.html',
    styleUrls: ['./dh-channel-list.component.scss']
})
export class DhChannelListComponent {

    @Input() channels;

    constructor() {
    }
}
