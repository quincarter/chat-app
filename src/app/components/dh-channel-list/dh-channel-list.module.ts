import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DhChannelListComponent } from './dh-channel-list.component';
import { MatListModule } from '@angular/material';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        DhChannelListComponent,
    ],
    imports: [
        CommonModule,
        MatListModule,
        RouterModule
    ],
    providers: [],
    exports: [
        DhChannelListComponent
    ]
})
export class DhChannelListModule {
}
