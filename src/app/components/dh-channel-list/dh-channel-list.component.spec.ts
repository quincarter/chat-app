import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DhChannelListComponent } from './dh-channel-list.component';

describe('DhChannelListComponent', () => {
    let component: DhChannelListComponent;
    let fixture: ComponentFixture<DhChannelListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DhChannelListComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DhChannelListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
