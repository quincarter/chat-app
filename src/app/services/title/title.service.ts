import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TitleService {

    private _title: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    public readonly chatTitle$: Observable<string> = this._title.asObservable();

    constructor() {
    }

    public setTitle(title: string): void {
        this._title.next(title);
    }
}
