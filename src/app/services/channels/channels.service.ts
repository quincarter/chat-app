import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { IChannel, IMessageContent } from '../../models/channels';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class ChannelsService {

    private _channelList: BehaviorSubject<any> = new BehaviorSubject(null);
    private _selectedChannel: BehaviorSubject<any> = new BehaviorSubject(null);

    public readonly channelList$: Observable<any> = this._channelList.asObservable();
    public readonly selectedChannel$: Observable<any> = this._selectedChannel.asObservable();

    constructor(
        private auth: AuthService,
        private db: AngularFirestore,
        private router: Router
    ) {
    }

    public getChannelList(): void {
        this._channelList.next(this.db.collection('channels').valueChanges());
    }

    public getSpecificChannel(channel_id: string): void {
        this._selectedChannel.next(this.db
            .collection<any>('/channels')
            .doc(channel_id).valueChanges()
        );
    }

    public async createChannel(newChannelName: string, data: IChannel): Promise<any> {
        const {uid} = await this.auth.getUser();

        data.created_by = uid;

        const channelRef = await this.db.collection('/channels').doc(newChannelName).set(data);

        return this.router.navigate(['channels', newChannelName]);
    }

    public async sendMessage(channelId: string, content: string) {
        const uid = 123;

        const data = {
            uid,
            content,
            createdAt: Date.now(),
        };

        if (uid) {
            const ref = this.db.collection('/channels').doc(channelId);
            return ref.update({
                messages: firebase.firestore.FieldValue.arrayUnion(data)
            });
        }
    }

    /*public joinUsers(channel$: Observable<any>) {
        let channel;
        const joinKeys = {};

        return channel$.pipe(
            switchMap(c => {
                channel = c;
                console.log('this is c ', c);
                const uids = Array.from(new Set(c.messages.map(v => v.uid)));

                const userDocs = uids.map(u =>
                    this.db.doc(`/users/${u}`).valueChanges()
                );

                return userDocs.length ? combineLatest([userDocs]) : of([]);
            }),
            map(arr => {
                arr.forEach(v => (joinKeys[(<any>v).uid] = v));
                channel.messages = channel.messages.map(v => {
                    return {...v, user: joinKeys[v.uid]};
                });
                return channel;
            })
        );
    }*/
}
