export interface IChannel {
    created_by: string;
    name: string;
    description?: string;
    members?: any;
    created_at: string;
    modified_at?: string;
    deleted_at?: string;
}

export interface IMessageContent {
    text: string;
    attachments: IAttachments;

}

export interface IAttachments {
    name: string;

}
