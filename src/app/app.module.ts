import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { APP_BASE_HREF } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { DhNavModule } from './components/dh-nav/dh-nav.module';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule, // imports firebase/firestore, only needed for database features
        AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
        AngularFireStorageModule,
        BrowserModule,
        BrowserAnimationsModule,
        DhNavModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        NgxAuthFirebaseUIModule.forRoot(environment.firebase),
        AppRoutingModule,
    ],
    providers: [{
        provide: APP_BASE_HREF,
        useValue: '/'
    }],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule {
}
