// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBNvXOEkq_Q7oMLmBwpQ5pNve0DPkeNKFU',
    authDomain: 'dahood-chat.firebaseapp.com',
    databaseURL: 'https://dahood-chat.firebaseio.com',
    projectId: 'dahood-chat',
    storageBucket: 'dahood-chat.appspot.com',
    messagingSenderId: '329055053045',
    appId: '1:329055053045:web:d160b2c2a068e120'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
